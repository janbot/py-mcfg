#include <iostream>
#include <sstream>
#include <cstdlib>
#include "utility.h"
#include <cassert>
#include <limits>
#include "nested_list.h"

using namespace mcfg;

int assert_compose(bool ought, const rvecs_t& holder, const IdxPairs& m, const rvec_t& passive,
              rvecs_t& partial_out) {

  std::cout << "  Compose " << passive << " according to m=" << m << " into " << holder << std::endl;
  bool ret = compose(holder, m, passive, partial_out);
  if (ret != ought) 
    std::cout << ">>>EXPECTED " << (ought ? "" : "no-") << "compose\n";
  return (int)(ret!=ought);
  
}

int test_single_lhs_arg() { 
// A(xyz) -> B(x,z) C(y) =>  m=[ ((0,0), (0,2)),  ((0,1))      ]  
  //Mapping m{ IdxPairs{ IdxPair{0,0}, IdxPair{0,2} }, IdxPairs{ IdxPair{0,1}   }     };
  Mapping m; int count=0;
  std::stringstream s("(((0 0) (0 2)) ((0 1)))");
  s >> m;
  auto holder = instantiate_result_holder(m);
  std::cout << "Mapping " << m << " implies holder " << holder << std::endl;
  std::cout << "-- Good ones --"  << std::endl; 
  rvec_t passive{ range_t(0,1), range_t(2,3) };
  rvecs_t outcome;
  count += assert_compose(true, holder, m[0], passive, outcome);
  rvecs_t next_out;
  count += assert_compose(true, outcome, m[1], rvec_t{range_t(1,2)}, next_out);
  return count;
}

int test_single_lhs_arg_bad() { 
// A(xyz) -> B(x,z) C(y) =>  m=[ ((0,0), (0,2)),  ((0,1))      ]  
  Mapping m; int count=0;
  std::stringstream s("(((0 0) (0 2)) ((0 1)))");
  s >> m;
  auto holder = instantiate_result_holder(m);
  std::cout << "Mapping " << m << " implies holder " << holder << std::endl; 
  rvec_t primary{ range_t(0,1), range_t(4,5) };
  rvecs_t out1;
  std::cout << "-- Good ones --"  << std::endl; 
  count += assert_compose(true, holder, m[0], primary, out1);

  std::cout << "-- Bad ones --"  << std::endl; 
  //test occupied
  rvecs_t dummy;
  count += assert_compose(false, out1, m[0], primary, dummy);

  //individual bad passive items to call with, bad wrt primary
  rvec_t bad_passives{ range_t(1,2), //right mismatch
                       range_t(3,4), //left mismatch
                       range_t(2,3), //whichever mismatch
                       range_t(0,4), //left overlap
                       range_t(1,5) //right overlap
                    };
  for (auto& passive_range : bad_passives) {
    rvecs_t next_out;
    count += assert_compose(false, out1, m[1], rvec_t{passive_range}, next_out);
  }
  return count;

}
int test_unary() {
  Mapping m; int count=0;
  std::stringstream s("(((0 0) (0 1) (0 2)))");
  s >> m;
  auto holder = instantiate_result_holder(m);
  std::cout << "Mapping " << m << " implies holder " << holder << std::endl;
  

  //good cases 
  {
  std::cout << "-- Good ones --"  << std::endl; 
  rvec_t child{ range_t(1,2), range_t(2,3), range_t(3,4) };
  rvecs_t dummy;
  count += assert_compose(true, holder, m[0], child, dummy);
  }

  std::cout << "-- Bad ones --"  << std::endl; 
  {
  rvec_t child{ range_t(0,1), range_t(2,3), range_t(3,4) }; //non-adjacency
  rvecs_t dummy;
  count += assert_compose(false, holder, m[0], child, dummy);
  }
  {
  rvec_t child{ range_t(2,3), range_t(0,2), range_t(3,4) }; //re-ordering that mismatches m
  rvecs_t dummy;
  count += assert_compose(false, holder, m[0], child, dummy);
  }
  
  //dimension mismatch is checked at a higher level
  //rvec_t child{ range_t(0,1), range_t(1,3)}; //dimension mismatch

  return count;
}

int test_two_lhs_args() {
  //// A(xy,z) -> B(x,z) C(y) =>  m=[ ((0,0), (1,0)),  ((1,1))      ]
  //Mapping m{ IdxPairs{ IdxPair{0,0}, IdxPair{1,0} }, IdxPairs{ IdxPair{0,1}   }     };
  Mapping m; int count=0;
  std::stringstream s("(((0 0) (1 0)) ((0 1)))");
  s >> m;
  auto holder = instantiate_result_holder(m);
  std::cout << "Mapping " << m << " implies holder " << holder << std::endl;
  
  std::cout << "-- Good ones --"  << std::endl; 
  rvec_t primary{ range_t(0,1), range_t(4,5) };
  rvecs_t outcome;
  count += assert_compose(true, holder, m[0], primary, outcome);

  //good cases 
  rvec_t good_passives{
                       range_t(1,2),
                       range_t(1,4),//OK to have end 4 with next range(4,5)
                       };
  for (auto& passive_range : good_passives) {
    rvecs_t dummy;
    count += assert_compose(true, outcome, m[1], rvec_t{ passive_range }, dummy);
  }

  std::cout << "-- Bad ones --"  << std::endl; 
  //individual bad passive items to call with, bad wrt primary
  rvec_t bad_passives{ 
                       range_t(2,3), //left mismatch
                       range_t(0,2), //left overlap
                       range_t(1,5) //overlap with next arg
                    };
  for (auto& passive_range : bad_passives) {
    rvecs_t next_out;
    count += assert_compose(false, outcome, m[1], rvec_t{passive_range}, next_out);
  }
  return count;
}

int test_three_lhs_args() {
  //// A(x,y,z) -> B(x,z) C(y) =>  m=[ ((0,0), (1,0)), ((2,0))      ]
  //Mapping m{ IdxPairs{ IdxPair{0,0}, IdxPair{1,0} }, IdxPairs{ IdxPair{0,1}   }     };
  Mapping m; int count=0;
  std::stringstream s("(((0 0) (2 0)) ((1 0)))");
  s >> m;
  auto holder = instantiate_result_holder(m);
  std::cout << "Mapping " << m << " implies holder " << holder << std::endl;
  
  std::cout << "-- Good ones --"  << std::endl; 
  rvec_t primary{ range_t(1,3), range_t(7,8) };
  rvecs_t outcome;
  count += assert_compose(true, holder, m[0], primary, outcome);

  //good cases 
  rvec_t good_passives{
                       range_t(4,6),
                       range_t(3,5), // no gap after prev arg is OK
                       range_t(5,7), // no gap before next arg is OK
                       range_t(3,7), // both gaps missing is OK
                       };
  for (auto& passive_range : good_passives) {
    rvecs_t dummy;
    count += assert_compose(true, outcome, m[1], rvec_t{ passive_range }, dummy);
  }

  std::cout << "-- Bad ones --"  << std::endl; 
  //individual bad passive items to call with, bad wrt primary
  rvec_t bad_passives{ 
                       range_t(1,5), // eats into prev arg
                       range_t(5,8), // eats into mext arg
                       range_t(0,5), // contains prev arg
                       range_t(5,9), // contains prev arg
                       range_t(0,1), // out of order - fully before prev arg
                       range_t(9,10), // out of order - fully after next arg
                     };
  for (auto& passive_range : bad_passives) {
    rvecs_t next_out;
    count += assert_compose(false, outcome, m[1], rvec_t{passive_range}, next_out);
  }
  return count;
}

int debug = 0;
int main(int argc, const char *argv[])
{ 
  int failcount = 0;  
  failcount += test_single_lhs_arg();
  std::cout << "------------" << std::endl;
  failcount += test_single_lhs_arg_bad();
  std::cout << "------------" << std::endl;
  failcount += test_two_lhs_args();
  std::cout << "------------" << std::endl;
  failcount += test_three_lhs_args();
  std::cout << "------------" << std::endl;
  failcount += test_unary();
  std::cout << "============" << std::endl;
  if (failcount > 0) 
    std::cout << "FAILED " << failcount << " times" << std::endl;
  else
    std::cout << "All passed " << std::endl;
  return 0;
}




