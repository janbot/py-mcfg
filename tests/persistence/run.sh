#!/bin/bash
# Run without commands to test against stored files
# Run with 'new' to create new reference files

if [[ $# -gt 0 && $1 == "new" ]]; then
  ext=
else
  ext=".hyp"
fi

rm -f log$ext
../../py-cfg -A final$ext -r 10101 -d 100 -N 1 -S model$ext -s 12 -F trace$ext -G wlt$ext -D -E -a 1e-4 -b 1e4 -e 1 -f 1 -g 10 -h 0.1 -w 1 -T 1 -m 0 -n 10 -R 0 grammar.g < input.yld &> log$ext

rm -f plog$ext
../../py-cfg -q -d 20000 -r 10101 -L -A reloaded$ext model10 < input.yld &> plog$ext

if [[ "$ext" == ".hyp" ]]; then
 diff -q final final.hyp
 diff -q trace trace.hyp
 diff -q <(sort wlt.hyp) <(sort wlt)
 
 cnt=1
 diff -q plog plog$ext &> /dev/null #  same => $? will equal 0

 # Give it a few tries -- there is some run-time dependence unrelated to actual model
 while [ $? -eq 1 ]; do
   if [[ $cnt -eq 10 ]]; then
     echo "No luck getting exact same parse"
     exit
   fi
   echo "$cnt"
   cnt=$((cnt+1))
   rm plog$ext
   ../../py-cfg -q -d 20000 -r 10101 -L -A reloaded$ext model10 < input.yld &> plog$ext
   diff -q reloaded.hyp reloaded &> /dev/null
 done

 diff -q reloaded.hyp reloaded
fi

