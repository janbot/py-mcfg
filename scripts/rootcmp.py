#!/usr/bin/python

usage = """Compute various root-based metrics for (tree, gold-roots) pairs

usage: %prog [options] < trees.txt 

Expects input data as one tree per line, and that nodes have their yield as part of the label.
Computes various metrics under the assumption of at most one hypothesised root morpheme per tree,
although gold data may have multiple roots (space-delimited) per line.

Gold data is in yield format (o__k). When an item has multiple gold candidates, the first one
is used for scoring, unless some other one exactly matches the hypothesis.
"""

import sys, optparse, re
from nltk.tree import Tree
from metrics import *
import formatting
from node_yields import map_trees

labelsep = formatting.labelsep
bigsep = formatting.bigsep
smallsep = formatting.smallsep

class Single_Root_Comparison:
  """ Aggregates metric objects for comparing single hypothesis root to single gold root """
  def __init__(self):
    self.rad_match = [ Accuracy("Rad Match (%d)\t" % (i+1)) for i in xrange(3)]
    self.exact_match = Accuracy("Exact Match\t")
    self.exact_match_ne = Accuracy("Exact Match*\t")
    self.radical_prf_macro = PrecRec("Radical F-score (macro)\t\t")
    self.radical_prf_ne_macro = PrecRec("Radical F-score (macro)*\t")

    self.whole_root_prf = PrecRec("Whole Root F-score (macro)\t")

  def compare(self, hyp, gold):
    """ hyp and gold are lists of radicals """
    if len(hyp) > 0:
      self.exact_match_ne.observe(hyp, gold)
      if len(gold) == 3 and len(hyp) == 3:
        for i in xrange(min(len(gold),len(hyp))):
          self.rad_match[i].observe(hyp[i], gold[i])
      self.radical_prf_ne_macro.observe(set(hyp), set(gold))

    self.whole_root_prf.observe( set( (hyp,)), set( (gold,)) ) 
    self.radical_prf_macro.observe(set(hyp), set(gold))

    self.exact_match.observe(hyp, gold) 

  def write(self):
    print "* = excludes null hypothesis from scoring"
    print self.exact_match
    print self.exact_match_ne
    for i in xrange(3):
      print self.rad_match[i]
    print self.whole_root_prf
    print self.radical_prf_macro
    print self.radical_prf_ne_macro
    

if __name__ == '__main__':
  op = optparse.OptionParser(usage=usage)
  op.add_option("-m", "--match", dest="node_re", help="regex to match node label on")
  op.add_option("-a", "--abort", dest="abort_res", help="space-delimited list of regexes to stop tree traversal if matched on node label", default="")
  op.add_option("-g", "--gold", dest="gold", help="gold data.")
  op.add_option("-s", "--skipemptygold", dest="skipemptygold", action="store_true", default=False,help="Skip over cases where gold root is blank")
  op.add_option("-v", "--verbose", dest="verbose", action="store_true", default=False, help="show errors")
  (options, args) = op.parse_args() 
  if len(args) > 1:
    sys.stderr.write("Error: too many command-line arguments\n")
    sys.exit(2)

  if len(args) == 1:
    inf = file(args[0], "rU")
  else:
    inf = sys.stdin
  inf = formatting.non_empty_lines(inf)

  node_rex = re.compile(options.node_re)
  abort_rexes = None
  if options.abort_res != "":
    abort_rexes = [re.compile(restr) for restr in options.abort_res.split(' ')]
  full_yields = []
  hyplist = []
  for rootnodelabel, matched_items in map_trees(inf, node_rex, abort_rexes):
    full_yields.append(rootnodelabel)

    assert all([len(r)==1 for m in matched_items for r in m])
    matched_items = [ tuple([r[0] for r in m]) for m in matched_items ]
    hyplist.append(matched_items)

  scores = Single_Root_Comparison()
  for (golds, hyps, yld) in zip(open(options.gold,'r'), hyplist, full_yields):
    golds = golds.strip()
    if options.skipemptygold and golds == '': 
      continue

    assert len(hyps) <= 1 #deal later with cases that match node_rex multiple times
    if len(hyps) > 0: 
      hyp = hyps[0]
    else:
      hyp = ()

    golds = golds.split(' ')
    gold = tuple(golds[0].split(bigsep)) #default to the first item
    if len(golds) > 1: #unless there's some other one that would be a match
      for g in golds:
        g = tuple(g.split(bigsep))
        if hyp == g:
          gold = g
          break          
      
    scores.compare(hyp, gold)
    if options.verbose:
      print str(int(hyp==gold)),
      print "In=\"%s\" Got=\"%s\" Expected=\"%s\"" % ( yld, bigsep.join(hyp), bigsep.join(gold))

  scores.write() 



