#!/usr/bin/python

usage = """%prog [options] < triples > ROCdata
Computes ROC data from the triples output by yieldextractor or postmarg_lex.py

Input format is <category> <yield> <prob> and does not need to be sorted
  Stem z_a_b_u_w_r_F_A 2.29257e-16
  StemChars a_E_a_l_u_w_^_A_@ 7.26492e-17
  Root l__u__w 7.26492e-17
The format of <yield> is such that _ separates terminals and __ separates sequences of terminals.

Gold format is one item per line, item ranges separated by __ but no separation between terminals.
e.g. k__t__b
     ktab
"""

import sys, optparse, re
import formatting as fmt

def parse_triples(triples_in, gold, target):
  """ Filter for the target category, judge correctness against gold set.
  Outputs: (bool, string, score) sorted by descending score, P, N
        where bool is whether it's right/wrong wrt gold.
    and P and N are the number of positives and negatives observed"""
  out = []
  P=0
  N=0
  for line in triples_in:
    if not line.startswith(target +' '): continue
    (cat, sss, p) = line.strip().split(' ')
    p = float(p)
    
    hyp = fmt.stryield2tuples(sss)
    assert len(hyp) > 0
    flag = (hyp in gold)
    out.append((flag, hyp, p))
    if flag:
      P+=1
    else:
      N+=1
  return sorted(out, key=lambda x: x[2], reverse=True), P, N

def top(judged_data, n=1):
  """ Generator to get the top n correct and top n wrong strings (ie. yields 2n strings)"""
  count = {True : 0, False : 0}
  for b, hyp, score in judged_data:
    if count[b] < n:
      count[b] += 1
      yield "%s %s %s" % (b and 'y' or 'n', fmt.tuples2stryield(hyp), str(score))
    if all( [c >= n for c in count.values()]):
      break

def roc(judged_data, P, N):
  """ Algorithm 2&3 from http://home.comcast.net/~tom.fawcett/public_html/papers/ROC101.pdf 
  Efficient and treats runs of the same score correctly.
  Returns list of ROC points and the AUC
  """
  # avoid division by zero
  if P == 0: P = 1e-30
  if N == 0: N = 1e-30

  FP=0
  TP=0
  rocdata = []
  prev = 0
  FPprev = 0
  TPprev = 0
  A = 0
  for (flag, dummy, score) in judged_data:
    if score != prev:
      rocdata.append(( float(FP)/N,float(TP)/P))
      A += traparea(FP, FPprev, TP, TPprev)
      FPprev = FP
      TPprev = TP
      prev = score
    if flag:
      TP += 1
    else:
      FP += 1

  rocdata.append(( float(FP)/N,float(TP)/P))
  A += traparea(N, FPprev, P, TPprev)
  A /= float(P*N)
  return rocdata, A

def traparea(X1, X2, Y1, Y2):
  base = float(abs(X1-X2))
  height = float(Y1+Y2) / 2.0
  return base * height

if __name__ == '__main__':
  op = optparse.OptionParser(usage=usage)
  op.add_option("-m", "--match", dest="target", help="Category to match in input")
  op.add_option("-g", "--gold", dest="gold", help="Set of gold strings (one per line; __ separates discontinuous parts)")
  op.add_option("-q", "--quiet", dest="quiet", action="store_true", default=False, help="don't show no graph, could still save it with -s")
  op.add_option("-v", "--verbose", dest="verbose", action="store_true", default=False, help="verbose")
  op.add_option("-a", "--auc", dest="auc", action="store_true", default=False, help="print auc only")
  op.add_option("-n", "--topn", dest="topn", default=0, help="print only top n strings (n correct + n wrong")
  op.add_option("-s", "--save", dest="savefile", default=None, help="save plot here")
  (options, args) = op.parse_args() 
  if len(args) > 1:
    sys.stderr.write("Error: too many command-line arguments\n")
    sys.exit(2)

  if len(args) == 1:
    inf = file(args[0], "rU")
  else:
    inf = sys.stdin

  gold = set([fmt.stryield2tuples(x.strip()) for x in open(options.gold,'rU').readlines()])

  pairs, P, N = parse_triples(inf, gold, options.target)
  rocdata, auc = roc(pairs, P, N)
  if options.verbose:
    sys.stderr.write("P=%d, N=%d\n" % (P,N))
    sys.stderr.write(str(pairs) + "\n")

  topn = int(options.topn)
  if options.auc:
    print "AUC =", auc
  else:
    if topn > 0:
      print '\n'.join( top(pairs, topn))
    else:
      for point in rocdata:
        print point[0], point[1]
  
  needgraph = not options.quiet or (not options.savefile is None and options.savefile !='')

  if needgraph:
    import matplotlib 
    if options.quiet:
      matplotlib.use("agg")
    import pylab as plt
    x = [x[0] for x in rocdata]
    y = [y[1] for y in rocdata]
    plt.plot(x, y, 'x-')
    plt.title("AUC = " + str(auc))
    if options.quiet:
      plt.savefig(options.savefile, format='png')
    else:
      plt.show()


