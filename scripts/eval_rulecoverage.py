#!/usr/bin/python

usage = """%prog -- computes coverage of a grammar wrt. a set of items

usage: %prog [options] < learned_grammar 

Expects input data as one rule per line, where cached rules
are of interest and are essentially trees (nodes have their yield as part of the label).
e.g. (R3#1__k__t__b (xxx ) )

Computes P/R/F for the set of extracted items against the set of gold items
"""

import sys, optparse, re
from nltk.tree import Tree
from metrics import *
import formatting
from node_yields import map_label

if __name__ == '__main__':
  op = optparse.OptionParser(usage=usage)
  op.add_option("-m", "--match", dest="node_re", help="regex to match tree's root node label on", default=r"R#")
  op.add_option("-g", "--gold", dest="gold", help="gold, in yield format")
  op.add_option("-v", "--verbose", dest="verbose", action="store_true", default=False, help="show errors")
  (options, args) = op.parse_args() 
  if len(args) > 1:
    sys.stderr.write("Error: too many command-line arguments\n")
    sys.exit(2)

  if len(args) == 1:
    inf = file(args[0], "rU")
  else:
    inf = sys.stdin

  node_rex = re.compile(options.node_re)

  hyps = set()
  for line in inf:
    line = line.strip()
    if len(line) == 0 or not (line[0]=='(' and line[-1]==')'): continue
    t = Tree(line)
    if node_rex.match(t.node): #only match against the root node of the tree
      hyps.add(map_label(t.node)) # get the terminal yield

  if not options.gold is None:
    golds = set([ formatting.stryield2tuples(gold.strip())  for gold in open(options.gold,'r')  ])
    if options.verbose:
      for h,g in zip(sorted(list(hyps)), sorted(list(golds))):
        print formatting.tuples2stryield(h), formatting.tuples2stryield(g), (h in golds and '1' or '0')
    prf = PrecRec()
    prf.observe(hyps, golds) 
    print prf

