from collections import defaultdict
class Accuracy:
  """Simple aggregator of statistics for accuracy"""
  def __init__(self, ident=""):
    self.total = 0
    self.correct = 0
    self.ident = ident

  def observe(self, hyp, gold):
    """Observe hyp and take it as correct if it equals gold"""
    self.total += 1
    if hyp == gold:
      self.correct += 1

  def __repr__(self):
    s=""
    if self.ident != "":
      s = self.ident + ": "
    return "%s%d/%d = %.2f%%" % (s, self.correct, self.total, self.correct / float(self.total + 1e-12) * 100.)


class PrecRec:

  def __init__(self, ident="", clipped = False, tp=0, fp=0, fn=0):
    self.tp = tp
    self.fp = fp
    self.fn = fn
    self.ident = ident
    self.clipped = clipped
    self.c=0

  def observe(self, hyp, gold):
    if self.clipped:
      self.__observe_clipped(hyp, gold)
    else:
      self.tp += len(gold.intersection(hyp))
      self.fn += len(gold.difference(hyp)) # things in gold that are not in hyp
      self.fp += len(hyp.difference(gold)) # things in hyp that are not in gold

  def __observe_clipped(self, hyp, gold):
    gold_c = dict([ (x, gold.count(x)) for x in set(gold)])
    hyp_c = dict([ (x, hyp.count(x)) for x in set(hyp)])
    
    def keyset(d):
      return set(d.keys())

    self.tp += sum([ min(gold_c[k],hyp_c[k]) for k in keyset(gold_c).intersection(keyset(hyp_c)) ])
    self.fn += sum([ gold_c[k] for k in keyset(gold_c).difference(keyset(hyp_c))])
    self.fp += sum([ hyp_c[k] for k in keyset(hyp_c).difference(keyset(gold_c))])

    if len(set(gold)) != len(gold): self.c+=1

  def __repr__(self):
    s=""
    if self.ident != "":
      s = self.ident + ": "
    return s + "TP %d, FP %d, FN %d; P %.4f, R %.4f, F %.4f" % (
                self.tp, self.fp, self.fn,
                self.P(),
                self.R(),
                self.F()
        ) # + (self.clipped and '*' or '') + ' ' + str(self.c)

  def P(self):
    """ Precision """
    return self.tp / ((self.tp + self.fp) + 1e-12)
  def R(self):
    """ Recall """
    return self.tp / ((self.tp + self.fn) + 1e-12)
  def F(self):
    """ F1 score """
    return 2.0 * self.tp / (2*self.tp + self.fp + self.fn + 1e-12)
