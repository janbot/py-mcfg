#!/usr/bin/python

import sys

def openfile(filename, i):
  return open('%s.%.3d' % (filename,i) ,'w')

counter = 1
filename = "out"
if len(sys.argv) > 1:
  filename = sys.argv[1]

content = 0
curfile = None
for line in sys.stdin:
  line = line.strip()
  if line == '':
    counter += 1
    curfile = None
    continue
  content += 1
  if curfile is None:
    curfile = openfile(filename, counter)
  curfile.write(line + '\n')


