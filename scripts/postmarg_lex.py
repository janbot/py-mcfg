#!/usr/bin/python

usage = """%prog -- Computes probabilities for (category, yieldstring) from multiple samples of trees, suitable for ROC.

usage: %prog [options] < tree-batches.txt > triples

Expects input data as one tree per line, and that nodes have their yield as part of the label.
Batches are separated by a blank line

Output format is same as yieldextractor's:
<cat> <yieldstr> <prob>
where <yieldstr> is like H__y__r or w_A_l.
"""

import optparse, re, sys
import node_yields
import formatting
from collections import Counter, defaultdict

def nlm(matched_str, node_label):
  """ Called during tree conversion -- returns (matched_str, yield_str)"""
  return matched_str, node_label.split(formatting.labelsep,1)[1]

def aggregate_counts(x):
  cat = x[0]
  counters = x[1]
  return cat, reduce(lambda y,z: y+z, counters)

def streamer(inf, N=0, I=0):
  """ For input in batches separated by a blank line,
  this is a generator that returns the first N
  items of the first I batches. 0 means no limit.
  Does not return the blank lines."""
  i=0 #sample
  n=-1 #input index
  for line in inf:
    line = line.strip()

    if line == '': # end of sample
      i+=1
      n=-1
      if I>0 and i>=I:
        break
      else:
        continue

    n += 1
    
    if N>0 and n>=N:
      continue

    yield line

def MapTreeList(tl):
  """ returns a dictionary {cat : [yld1, yld2, ...]} aggregated over the trees in tl """
  #print 'M', tl
  cat2matches = defaultdict(Counter)
  for matchpairs in map(tm.map_tree, tl): # for each tree, gives a list of match-pairs
    for (cat,yld) in set(matchpairs):     # but only consider the unique set of match-pairs otherwise 
      cat2matches[cat].update((yld,))     # the final result is not a correctly normalised probability
  
  return cat2matches

def chunks(l, n):
  """ Split list l into chunks of length n """
  for i in xrange(0, len(l), n):
    yield l[i:i+n]

if __name__ == '__main__':
  op = optparse.OptionParser(usage=usage)
  op.add_option("-m", "--match", dest="node_re", help="regex to match node label on", default=r"R#")
  op.add_option("-a", "--abort", dest="abort_res", help="space-delimited list of regexes to stop tree traversal if matched on node label", default="")
  op.add_option("-v", "--verbose", dest="verbose", help="verbose", default=False, action="store_true")
  op.add_option("-n", "--firstn", dest="firstn", help="Only compute first n outcomes (across unchanged number of samples)", default=0)
  op.add_option("-i", "--samples", dest="samples", help="Use this many samples (0 means all)", default=0)
  op.add_option("-P", "--procs", dest="procs", help="Use this many processes", default=1)
  (options, args) = op.parse_args() 
  if len(args) > 1:
    sys.stderr.write("Error: too many command-line arguments\n")
    sys.exit(2)

  if len(args) == 1:
    inf = file(args[0], "rU")
  else:
    inf = sys.stdin

  node_rex = re.compile(options.node_re)
  abort_rexes = None
  if options.abort_res != "":
    abort_rexes = [re.compile(restr) for restr in options.abort_res.split(' ')]
  
  tm = node_yields.TreeMapper(node_rex, abort_rexes, nlm)
  firstn = int(options.firstn)
  samples = int(options.samples)
  procs = int(options.procs)

  if procs > 1:
    from multiprocessing import Pool
    pool = Pool(procs)
    mapfunc = pool.map
  else:
    mapfunc = map

  inputs = list(streamer(inf, firstn, samples))
  total = len(inputs)
  treechunks = chunks(inputs, total/procs) #chunksize
  #print list(treechunks)

  observations = mapfunc(MapTreeList, treechunks)
  #print len(observations),observations
  
  assert len(observations)>0
  cats = set(observations[0].keys())
  #print cats

  counts_by_cat = []
  for cat in cats:
    counts_by_cat.append( (cat, [obchunk[cat] for obchunk in observations]))
  #print counts_by_cat

  counts_by_cat = mapfunc(aggregate_counts, counts_by_cat)

  total = float(total) + 1e-30
  for (cat, counts) in counts_by_cat:
    for yld,cnt in counts.iteritems():
      print cat,yld, cnt/total
    










#  data = group_corr(list(read_multi(inf)))
#  if int(options.firstn) > 0:
#    data = data[:int(options.firstn)]
#
#  accum = Counter()
#  accum = []
#  accumd = defaultdict(list)
#  denom = 1e-20
#  for i,analysed_sentence in enumerate(data): # analysed_sentence is a list of all the analysis for input string i
#    if options.verbose:
#      print "\nInput", i, analysed_sentence
#
#    counts = reduce(lambda x,y: x+y, map(lambda x: tm.map_tree(x), analysed_sentence))
#    for (cat,yld) in counts:
#      accumd[cat].append(yld)
#    #counts = map(lambda x: Counter(tm.map_tree(x)), analysed_sentence)
#    denom += len(analysed_sentence) # number of samples for this input string 
#    if options.verbose:
#      print counts
#      print '\t',reduce(lambda x,y: x+y, counts)
#    #accum += reduce(lambda x,y: x+y, counts)
#    accum.append(counts)
# 
#  if options.verbose:
#    print "Observations (i.e. denominator):", denom
#    print accum
#  sys.stderr.write("Reducing..\n")
#  accum = reduce(lambda x,y: Counter(x)+Counter(y), accum) 
#  
#  results = [ (cat, Counter(lst))    for (cat, lst) in accumd.iteritems()]
##  print accum
##  for ((cat, yld), cnt) in accum.iteritems():
##    print cat, yld, cnt/denom

