#!/usr/bin/python

usage = """%prog -- given a tree, extract the yield(s) of node(s) matching a regex

usage: %prog [options] < trees.txt > part-yields.txt

Expects input data as one tree per line, and that nodes have their yield as part of the label.
For each node that matches the label_regex, it prints f(label), possibly multiple times per tree.

"""

import sys, optparse, re
from nltk.tree import Tree
import formatting

labelsep = formatting.labelsep
bigsep = formatting.bigsep
smallsep = formatting.smallsep

def map_label(label):
  """ Maps a node lable Cat__Y_i__e_l_d to the tuple representation, dropping Cat.
  e.g. ((Y,i),(e,l,d))"""
  terminals = label.split(labelsep, 1)[1]
  return formatting.stryield2tuples(terminals)

class TreeMapper:
  def __init__(self, node_rex, abort_rexes=None, node_label_mapper = None):
    """ 
    node_label_mapper is a function taking arguments (matchedstr, nodelabel)
      matchedstr is the full matched regex, unless the regex had a group capture,
      in which case the first captured group is taken as matchedstr
    node_rex is a single regex or a list of them
    """
    if isinstance(node_rex, list) or isinstance(node_rex, tuple):
      self.node_rexes = node_rex
    else:
      self.node_rexes = (node_rex,)
    
    self.abort_rexes = abort_rexes

    if node_label_mapper is None:
      self.node_label_mapper = lambda x,y: map_label(y)
    else:
      self.node_label_mapper = node_label_mapper

  def map_tree(self, tree):
    if not isinstance(tree, Tree):
      tree = Tree(tree)

    def visit(node, aggregator):
      if isinstance(node, list): # check that node is non-terminal

        # stop traversal if node matches something in the abort_rexes
        if not self.abort_rexes is None:
          for rex in self.abort_rexes:
            if rex.match(node.node):
              #print "Aborting at ", node
              return
        # do actual matching, adding to aggregator
        for node_rex in self.node_rexes:
          mo = node_rex.match(node.node)
          if mo:
            matched_str = len(mo.groups())>0 and mo.groups()[0] or mo.group()
            aggregator.append(self.node_label_mapper(matched_str, node.node)) 
        for child in node:
          visit(child, aggregator)

    results = []
    visit(tree, results)
    return results

def map_trees(trees, node_rex, abort_rexes = None):
  """ Generator that yields for each tree the pair (tree-yield, matched-items),
  where matched-items is a list of yields from nodes that matched node_rex, subject to abort_rexes
  and in tuples format. 'tree-yield' is simply the whole node label, unchanged from input
  Example return pair:
     W_o_r_d, [ match1, match2], where match1=((o,r,d)), match2=((W),(d)) for example.
  """
  tm = TreeMapper(node_rex, abort_rexes)
  for line in trees:
    assert len(line) > 0

    t = Tree(line)
    extracted = tm.map_tree(t)
    yield t.node, extracted

if __name__ == '__main__':
  op = optparse.OptionParser(usage=usage)
  op.add_option("-m", "--match", dest="node_re", help="regex to match node label on", default=r"R#")
  op.add_option("-a", "--abort", dest="abort_res", help="space-delimited list of regexes to stop tree traversal if matched on node label", default="")
  op.add_option("-g", "--gold", dest="gold", help="gold data. If unspecified, simply prints out the list of matches per input item")
  (options, args) = op.parse_args() 
  if len(args) > 1:
    sys.stderr.write("Error: too many command-line arguments\n")
    sys.exit(2)

  if len(args) == 1:
    inf = file(args[0], "rU")
  else:
    inf = sys.stdin

  inf = formatting.non_empty_lines(inf)
  node_rex = re.compile(options.node_re)
  abort_rexes = None
  if options.abort_res != "":
    abort_rexes = [re.compile(restr) for restr in options.abort_res.split(' ')]
  
  for (full_yield, matched_items) in map_trees(inf, node_rex, abort_rexes):
    print full_yield, ';'.join([formatting.tuples2stryield(matched_item) for matched_item in matched_items ])




