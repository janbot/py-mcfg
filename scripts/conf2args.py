#!/usr/bin/python

""" Transform a config file into a line of arguments.
Input example:
  #comments allowed like this, and after contents
  key = val #matches single token either side of
  flag #simple flag
Output:
  -key val -flag

Intended use: ./somebinary `conf2args.py < configfile` other args < in > out
"""

import sys, re

# both regexes here won't match empty or comments and won't capture comments at end of line.
# order in which they're applied matters
kv_re = re.compile(r'\s*(\S+?)\s*=\s*(\S+?)\s*(?:#.*)?$').match #key-value
flag_re = re.compile(r'[^\S#]*(\S+?)\s*(?:#.*)?$').match #flag
flags=[]
kv=[]
for line in sys.stdin:
  line = line.strip()
  if re.match(r'\s*#', line): continue

  mo = kv_re(line)
  if mo:
    kv.append(mo.groups())
  else:
    mo = flag_re(line)
    if mo:
      flags.append(mo.groups()[0])

for el in kv:
  print '-%s %s' % el,
for flag in flags:
  print '-%s' % flag,

print
