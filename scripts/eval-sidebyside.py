#!/usr/bin/python

usage = """%prog -- Computes exact-match accuracy over side-by-side matching files

usage: %prog [options] -g gold hypdata
Expects input data as one string per line

The flags adapt the functionality based on the gold contents.
"""

import sys, optparse, re
from metrics import *
from formatting import bigsep, stryield2tuples

if __name__ == '__main__':
  op = optparse.OptionParser(usage=usage)
  op.add_option("-g", "--gold", dest="gold", help="gold")
  op.add_option("-i", "--ignoreblankgold", dest="skipblankgold", default=False, action="store_true", help="skip blank gold lines")
  op.add_option("-l", "--lenient", dest="lenient", default=False, action="store_true", help="find match among multiple golds in a line")
  op.add_option("-a", "--ignoreambiguous", dest="ign_amb", default=False, action="store_true", help="ignore line if gold is ambiguous")
  op.add_option("-v", "--verbose", dest="verbose", action="store_true", default=False, help="show errors")
  (options, args) = op.parse_args() 
  if len(args) > 1:
    sys.stderr.write("Error: too many command-line arguments\n")
    sys.exit(2)

  if len(args) == 1:
    inf = file(args[0], "rU")
  else:
    inf = sys.stdin

  skipblankgold = options.skipblankgold
  lenientgold = options.lenient
  acc = Accuracy("Accuracy")
  for hypline, goldline in zip(inf, open(options.gold,'r')):
    hyp = hypline.strip()
    gold = goldline.strip() 
    if gold == '' and skipblankgold: continue
    if options.ign_amb and len(gold)>1 and len(gold.split())>1: continue

    if len(hyp.split()) > 1:
      sys.stderr.write("More than one item on the hyp side! '" + hyp + "'")
      sys.exit()
   
    if lenientgold and gold != '':
      gold = gold.split()
      if len(gold) > 1 and hyp in gold:
        gold = gold[gold.index(hyp)]
      else:
        gold = gold[0]

    if options.verbose:
      print str(hyp==gold) + "\tgot '" + hyp + "'\texpected '" + gold + "'"
    acc.observe(hyp, gold)

  print acc, "\t(skipblankgold=" + str(skipblankgold) + ", lenientgold=" + str(lenientgold) + ", ignoreambiguous=" + str(options.ign_amb) +")"
