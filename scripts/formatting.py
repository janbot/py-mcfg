labelsep = "__" #in node label, this separates category from yield 
bigsep ="__" #delimiter between different ranges in a node's yield
smallsep ="_" #delimiter between terminals in a single range

debug = False
def stryield2rangetuples(strvec):
  """ Converts a_a__b_b to (aa, bb) """
  if debug: print strvec, 
  ret = tuple([ part.replace(smallsep,'')  for part in strvec.split(bigsep)  ])
  if debug: print ret
  return ret

def stryield2tuples(strvec):
  """ Converts a_a__b_b to ( (a,a), (b,b)) """
  if debug: print strvec, 
  ret = tuple([ tuple(part.split(smallsep))  for part in strvec.split(bigsep)  ])
  if debug: print ret
  return ret

def rangetuples2stryield(rtuples):
  """ Converts (aa, bb) to a_a__b_b """
  if debug: print rtuples, 
  ret = bigsep.join([smallsep.join(list(x))  for x in rtuples])
  if debug: print ret
  return ret

def tuples2stryield(tuples):
  """ Converts ((a,a), (b,b)) to a_a__b_b """
  if debug: print tuples, 
  ret = bigsep.join([smallsep.join(x) for x in tuples])
  if debug: print ret
  return ret

def non_empty_lines(iterable):
  for line in iterable:
    line = line.strip()
    if line != '':
      yield line
