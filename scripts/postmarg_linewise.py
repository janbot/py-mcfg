#!/usr/bin/python

usage = """%prog -- produces a single outcome per training instance by marginalising over a set of trees for each

usage: %prog [options] < tree-batches.txt > single-outcomes.txt

Expects input data as one tree per line, and that nodes have their yield as part of the label.
Batches are separated by a blank line

The kind of 'outcome' is determined by the options passed.
- Default mode:
  Returns argcountmax over sets of matches determined by -m, separating elements by tabs.
     e.g. for a tree encoding (abc/x def/y), -m 'x' would give a_b_c, and -m 'x|y' would give a_b_c<tab>d_e_f
- Segmentation mode (-s):
    prints out most frequent segmentation for each instance

When -d is passed, these actions are performed over only the single most frequent derivation per instance.
This abstracts over the counts that may be present in node labels, e.g. Stem#31 is read as Stem.
Pass -p to print out those max derivations and does nothing else.
"""

import optparse, re, sys
import node_yields
import formatting
from collections import Counter

def read_multi(inf):
  batch = []
  for line in inf:
    line = line.strip()
    if line == '':
      yield batch
      batch = []
    else:
      batch.append(line)

def group_corr(data):
  """ Groups corresponding items,
  e.g. transforms [ [1, 2, 3],  [11, 22, 33] ]
  into [ [1,11], [2,22], [3,33]]"""
  return map(None, *data)

def argmaxcount(items):
  if len(items)>0:
    return Counter(items).most_common(1)[0][0]
  else:
    return None
  
def map_analysis_to_seg(yieldtuples):
  """Converts each t in yieldtuples into its stringyield format, removing the smallseps and joining the results into
  a space delimited string. I.e. intended use is that yieldtuples contain morphs and that this function
  converts them into a space-segmented string"""
  return ' '.join([formatting.tuples2stryield(x).replace(formatting.smallsep,'') for x in yieldtuples])

def flatten_matches(analysis):
  return '\t'.join([formatting.tuples2stryield(x) for x in analysis])

if __name__ == '__main__':
  op = optparse.OptionParser(usage=usage)
  op.add_option("-m", "--match", dest="node_re", help="regex to match node label on", default=r"R#")
  op.add_option("-a", "--abort", dest="abort_res", help="space-delimited list of regexes to stop tree traversal if matched on node label", default="")
  op.add_option("-s", "--segm", dest="segm", help="segmentation mode -- marginalises out distinct derivations that imply the same segmentation", default=False, action="store_true")
  op.add_option("-d", "--deriv", dest="deriv", help="derivation mode -- get most common derivation for each input string", default=False, action="store_true")
  op.add_option("-v", "--verbose", dest="verbose", help="verbose", default=False, action="store_true")
  op.add_option("-p", "--print", dest="dprint", help="print most common derivation per input and do nothing else. Only has effect in conjunction with -d", default=False, action="store_true")
  op.add_option("-n", "--firstn", dest="firstn", help="Only compute first n outcomes (across unchanged number of samples)", default=0)
  (options, args) = op.parse_args() 
  if len(args) > 1:
    sys.stderr.write("Error: too many command-line arguments\n")
    sys.exit(2)

  if len(args) == 1:
    inf = file(args[0], "rU")
  else:
    inf = sys.stdin

  node_rex = re.compile(options.node_re)
  abort_rexes = None
  if options.abort_res != "":
    abort_rexes = [re.compile(restr) for restr in options.abort_res.split(' ')]
  
  tm = node_yields.TreeMapper(node_rex, abort_rexes)
  data = group_corr(list(read_multi(inf)))
  if int(options.firstn) > 0:
    data = data[:int(options.firstn)]
  for i,analysed_sentence in enumerate(data): # analysed_sentence is a list of all the analysis for input string i
    if not (isinstance(analysed_sentence, list) or isinstance(analysed_sentence, tuple)):
      analysed_sentence = (analysed_sentence,) # this has something to do with group_corr messing up for single
    if options.verbose:
      print "\nInput", i, analysed_sentence, len(analysed_sentence)
    if options.deriv:
      maxmarginal_derivation = argmaxcount(map(lambda x: re.sub('#\d+','', x), analysed_sentence))
      analysed_sentence = (maxmarginal_derivation,)
      if options.dprint:
        print maxmarginal_derivation
        continue

    if options.segm:
      segmentations = map(lambda x: map_analysis_to_seg(tm.map_tree(x)), analysed_sentence)
      if options.verbose:
        print '\t', segmentations
      print argmaxcount(segmentations)
    else:
      analyses = map(lambda x: flatten_matches(tm.map_tree(x)), analysed_sentence)
      if options.verbose:
        print analyses
      print argmaxcount(analyses)

