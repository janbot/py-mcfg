#!/usr/bin/python

usage = """%prog -- Computes precision/recall/F-score over two sets of strings

usage: %prog [options] -g golddata hypdata

Expects input data as one element per line """

import sys, optparse
from metrics import *

if __name__ == '__main__':
  op = optparse.OptionParser(usage=usage)
  op.add_option("-g", "--gold", dest="gold", help="gold")
  op.add_option("-v", "--verbose", dest="verbose", action="store_true", default=False, help="show errors")
  (options, args) = op.parse_args() 
  if len(args) > 1:
    sys.stderr.write("Error: too many command-line arguments\n")
    sys.exit(2)

  if len(args) == 1:
    inf = file(args[0], "rU")
  else:
    inf = sys.stdin

  hyps = set([ hyp.strip() for hyp in inf  ])
  golds = set([ gold.strip() for gold in open(options.gold,'r')  ])

  if options.verbose:
    print "Some Hyps", list(hyps)[:10]
    print "Some Golds", list(golds)[:10]

  prf = PrecRec()
  prf.observe(hyps, golds) 
  print prf
