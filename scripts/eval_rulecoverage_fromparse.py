#!/usr/bin/python

usage = """%prog -- computes coverage of a grammar wrt. a set of items

usage: %prog [options] < parsed_data

Expects input data as one parse tree per line

"""

import sys, optparse, re
from nltk.tree import Tree
from node_yields import TreeMapper
from metrics import *
from formatting import bigsep, stryield2tuples

if __name__ == '__main__':
  op = optparse.OptionParser(usage=usage)
  op.add_option("-m", "--match", dest="node_re", help="regex to match root node label on", default=r"R#")
  op.add_option("-g", "--gold", dest="gold", help="gold")
  #op.add_option("-v", "--verbose", dest="verbose", action="store_true", default=False, help="show errors")
  (options, args) = op.parse_args() 
  if len(args) > 1:
    sys.stderr.write("Error: too many command-line arguments\n")
    sys.exit(2)

  if len(args) == 1:
    inf = file(args[0], "rU")
  else:
    inf = sys.stdin

  node_rex = re.compile(options.node_re)

  hyps = set()
  tm = TreeMapper(node_rex)
  for line in inf:
    line = line.strip()
    if len(line) == 0 or line[0]!='(' or line[-1]!=')' : continue
    t = Tree(line)
    matches = tm.map_tree(Tree(line))
    hyps.update(set(matches))

  if not options.gold is None:
    golds = set([ stryield2tuples(gold.strip())  for gold in open(options.gold,'r')  ])
    prf = PrecRec()
    print hyps, golds
    prf.observe(hyps, golds) 
    print prf

