#!/usr/bin/python

import sys
from nltk.tree import Tree

def main(treein):
  for line in treein:
    line = line.strip()
    t = Tree(line)
    print t.pprint_latex_qtree()
    t.draw()

if __name__ == '__main__':
  main(sys.stdin)
